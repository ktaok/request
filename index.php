<?php require_once ("../../lib/init.php"); //設定ファイル 全ページに入れる ?>
<?php require("../../lib/memberlib.php"); //会員判別・会員認証するページに入れる ?>
<?php
// パーソナル情報を取得
$personal = personal();
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>リクエスト｜<?php echo $site_name; ?></title>
<meta name="keywords" content="リクエスト,REQUEST">
<meta name="description" content="<?php echo $site_name; ?> リクエスト">
<!--[if lt IE 9]><script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">
<meta name="format-detection" content="telephone=no">
<link rel="stylesheet" href="../css/import.css" type="text/css" media="all">
<link rel="stylesheet" href="../css/request.css" type="text/css" media="all">
<link href="/img/favicon_iphone.png" rel="apple-touch-icon" />
<link href="/img/favicon.png" rel="shortcut icon" type="image/png" />
<link href="/img/favicon.png" rel="icon" type="image/png" />
<!--#include virtual="/ssi/ga.html"-->
</head>
<body id="support">

<!--#include virtual="/ssi/header.html"-->

<!-- CONTENT -->
<article>
<section id="request">
<h2>リクエスト</h2>

<p>「<?php echo $site_name; ?>」では皆様からのリクエストを随時受け付けております。お客様が満足できるサービスを提供できるよう、スタッフ一同努力してまいります。<br>
「こんなコンテンツを扱って欲しい」などご意見をお待ちしております。</p>

<form action="/cgi-bin/request/confirm.cgi" method="post">
<fieldset>
<legend>リクエストフォーム</legend>
<dl>
<dt><label for="age">年代</label></dt>
<dd><select name="age" id="age" required>
<option value="" selected>選択してください</option>
<option value=""></option>
<option value="10代">10代</option>
<option value="20代">20代</option>
<option value="30代">30代</option>
<option value="40代">40代</option>
<option value="50代">50代</option>
</select></dd>
<dt>性別</dt>
<dd><label for="male"><input type="radio" name="sex" value="男性" id="male" />&nbsp;男性</label>&nbsp;&nbsp;&nbsp;<label for="female"><input type="radio" name="sex" value="女性" id="female" />&nbsp;女性</label></dd>
<dt><label for="comment">ご意見・ご要望（必須）</label></dt>
<dd><textarea id="comment" name="comment" required></textarea></dd>
</dl>
</fieldset>
<input type="submit" value="入力内容を確認する" class="btn">
</form>

</section>
</article>
<!-- //CONTENT -->

<!--#include virtual="/ssi/footer.php"-->

</body>
</html>
